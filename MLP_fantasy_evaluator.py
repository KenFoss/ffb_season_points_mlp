import numpy as np
from tensorflow import keras
from keras.models import Model
from keras.layers import Input, Dense, concatenate

import subprocess
import os
import shlex
import argparse
import doctest

import format_data


def create_dataset(low_year, high_year):
    '''
    The purpose of this method is to take the 
    
    Keyword arguments:
    low_year -- int, the lower bound of data presented for training
    high_year -- int, the upper bound of data presented for training

    >>> create_dataset(2022, 2022)

    '''

    #Create object of DATA_PROCESSOR()
    processor = format_data.DATA_PROCESSOR()

    current_year = low_year

    #INPUTS = [PLAYER, REST OF TEAM OL GRADE, REST OF TEAM REC, REST OF TEAM PASSING, REST OF TEAM RUN OTHER STATS]

    while(current_year <= high_year):
        #get the dicts for the current year (low_year)
        data_list = [ 'passing_stats_filtered_'+current_year+'.csv', 'receiving_stats_filtered_'+current_year+'.csv', 'rushing_stats_filtered_'+current_year+'.csv' ]

        yearly_data = processor.read_csvs(data_list, {})

        #for each team there are positions
        #       LOOP THROUGH POSITIONS
        #       for each team a sum for each position is made                                            ####
        #           [OL_SUM, WR_SUM, PASS_SUM, RUN_SUM]                                                         
        #           [OL_NUM, WR_NUM, PASS_NUM, RUN_NUM]                                                                     
        #       At the same time, we queue up players at these positions                                                       
        #           [ BLANK, WR_PLAYERS, QB_PLAYERS, RB_PLAYERS]\                                                       
        #                                                                                                              
        #       It is important these lists be in order as we will                                                        
        #         then deque players and subtract their grade                                                       
        #         from the sum. This will be the grade for their                                                        
        #         position group (also in the same order in data).                                                       
        #        
        #        FOR ALL QUEUES
        #        while( not WR_PLAYERS_QUEUE is empty):
        #           THIS_GRADE = (WR_SUM - WR_PLAYER_GRADE) / WR_NUM - 1
        #           add to INPUT list [P, OLG, THIS_GRADE, PG,RG, PLAYER_OTHER_STATS(not fantasy pts)]
        #           add to OUTPUT list [p, fantasy pts]
        #
        # NEXT TEAM


class CSVFolderAction(argparse.Action):
    '''
    This sloppy file action will get a list of the files in the current folder
    '''

    def __call__(self, parser, namespace, values, option_string=None):
        
        folder_path = values

        data = os.listdir(folder_path)
            
        print("The following data has been located: \n" + data)

        setattr(namespace, self.dest, data)

if __name__ == '__main__':
    '''
        main
    '''
    parser = argparse.ArgumentParser()
    # Add the argument with the custom action
    parser.add_argument('--folder', action=CSVFolderAction, dest='csv_files', help='Path to the folder containing CSV files')
    parser.add_argument('--method', help='pass method name to doctest this specific case')

    # Parse the command-line arguments
    args = parser.parse_args()


    ##Run Doctests
    if args.method:
        doctest.run_docstring_examples(getattr(args.method), globals(), optionflags=doctest.ELLIPSIS)
