#!/usr/bin/env python3

import argparse
import doctest

import shlex
import subprocess

import csv
from queue import Queue
import pandas as pd
import sys
import os

wr_csv = 'wr_stats.csv'


class DATA_PROCESSOR:

    def sort_csvs(self, csv_type, year):
        '''
        This method takes the csvs of specified type, and year, filtering them for the propper input to 'read_csvs()'

        csv_type => String, options are 'rec', 'rush', 'pass'.
        year => String, the year of the data.

        >>> obj = DATA_PROCESSOR()
        >>> obj.sort_csvs("rec", "2022")
        >>> obj.sort_csvs("pass", "2022")
        >>> obj.sort_csvs("rush", "2022")
        >>> obj.read_csvs(['filtered_data/passing_stats_filtered_2022.csv','filtered_data/rushing_stats_filtered_2022.csv','filtered_data/receiving_stats_filtered_2022.csv'], {})
        
        '''

        # if fantasy_stats_<YEAR> does not exist... then we have the split data and need to merge it
        # This means we also need to remove quotes from around the entries 

        if not os.path.isfile('feeder_data/fantasy_stats_' + year + '.csv'):

            #remove quotes from csvs... Entries are in this form originally "<STAT>"
            input_file = "input.csv"
            output_file = "output.csv"

            df_rec = pd.read_csv('feeder_data/fantasy_stats_rec_' + year + '.csv', quotechar='"')
            df_pass = pd.read_csv('feeder_data/fantasy_stats_pass_' + year + '.csv', quotechar='"')

            # Remove quotes from entries
            df_rec = df_rec.applymap(lambda x: x.strip('"') if isinstance(x, str) else x)
            df_pass = df_pass.applymap(lambda x: x.strip('"') if isinstance(x, str) else x)

            # Save the modified DataFrame to a new CSV file
            df_rec.to_csv('feeder_data/fantasy_stats_rec_' + year + '.csv', index=False)
            df_pass.to_csv('feeder_data/fantasy_stats_pass_' + year + '.csv', index=False)

            #limit these to just player and fantasyPts then merge
            df1 = pd.read_csv('feeder_data/fantasy_stats_rec_' + year + '.csv')
            df1 = df1[['Player', 'Fantasy Points Pts']]
            df2 = pd.read_csv('feeder_data/fantasy_stats_pass_' + year + '.csv')
            df2 = df2[['Player', 'Fantasy Points Pts']]
            merged_fantasy_df = pd.merge(df1, df2, on='Player', how='outer')
            merged_fantasy_df['Fantasy Points Pts'] = merged_fantasy_df['Fantasy Points Pts_x'].fillna(merged_fantasy_df['Fantasy Points Pts_y'])
            merged_fantasy_df = merged_fantasy_df.drop(['Fantasy Points Pts_x', 'Fantasy Points Pts_y'], axis=1)

            merged_fantasy_df.to_csv('feeder_data/fantasy_stats_' + year + '.csv', index=False)
        if(csv_type == "rec"):
            # Read the CSV file
            df = pd.read_csv('feeder_data/receiving_stats_' + year + '.csv')
            df_fantasy = pd.read_csv('feeder_data/fantasy_stats_' + year + '.csv')

            # Filter rows based on the condition: route_rate >= 90%
            df = df[df['route_rate'] >= 0.9]

            # Select only the desired columns
            columns_to_keep = ['player', 'position', 'team_name', 'grades_offense',
                            'contested_catch_rate', 'caught_percent']
            df = df[columns_to_keep]

            # Calculate the average of contested_catch_rate and caught_percentage as a new column
            df['custom_stats'] = (pd.to_numeric(df['contested_catch_rate'], errors='coerce') + pd.to_numeric(df['caught_percent'], errors='coerce')) / 2

            # Remove the unnecessary columns
            columns_to_remove = ['contested_catch_rate', 'caught_percent']
            df.drop(columns=columns_to_remove, inplace=True)

            df['year'] = year

            # Merge 'fantasy_rank' column from df_fantasy based on 'player'
            df = df.merge(df_fantasy[['Player', 'Fantasy Points Pts']], left_on='player', right_on='Player', how='left')

            # Drop the redundant 'Player' column from the merged DataFrame
            df.drop('Player', axis=1, inplace=True)

            # Save the filtered and selected data to a new CSV file
            df.to_csv('filtered_data/receiving_stats_filtered_' + year + '.csv', index=False)
        if(csv_type == "rush"):
            # Read the CSV filea
            df = pd.read_csv('feeder_data/rushing_stats_' + year + '.csv')
            df_fantasy = pd.read_csv('feeder_data/fantasy_stats_' + year + '.csv')

            # Filter rows based on the condition: run_plays >= 70
            df = df[df['run_plays'] >= 70]

            # Select only the desired columns
            columns_to_keep = ['player', 'position', 'team_name','grades_offense',
                            'breakaway_percent', 'yards_after_contact', 'yards']
            df = df[columns_to_keep]

            # Calculate the average of breakaway_percent and yards_after_contact/yards as a new column
            df['custom_stats'] = (pd.to_numeric(df['breakaway_percent'], errors='coerce') + ( (pd.to_numeric(df['yards'], errors='coerce') / pd.to_numeric(df['yards'], errors='coerce')) ) ) / 2 

            # Remove the unnecessary columns
            columns_to_remove = ['breakaway_percent', 'yards_after_contact', 'yards']
            df.drop(columns=columns_to_remove, inplace=True)

            df['year'] = year

            # Merge 'fantasy_rank' column from df_fantasy based on 'player'
            df = df.merge(df_fantasy[['Player', 'Fantasy Points Pts']], left_on='player', right_on='Player', how='left')

            # Drop the redundant 'Player' column from the merged DataFrame
            df.drop('Player', axis=1, inplace=True)

            # Save the filtered and selected data to a new CSV file
            df.to_csv('filtered_data/rushing_stats_filtered_' + year +'.csv', index=False)
        if(csv_type == "pass"):
            # Read the CSV filea
            df = pd.read_csv('feeder_data/passing_stats_' + year + '.csv')
            df_fantasy = pd.read_csv('feeder_data/fantasy_stats_' + year + '.csv')

            # Filter rows based on the condition: run_plays >= 70
            df = df[df['attempts'] >= 70]

            # Select only the desired columns
            columns_to_keep = ['player', 'position', 'team_name','grades_offense',
                            'accuracy_percent', 'btt_rate', 'twp_rate', 'pressure_to_sack_rate']
            df = df[columns_to_keep]

            # Calculate the average of breakaway_percent and yards_after_contact/yards as a new column
            df['custom_stats'] = (pd.to_numeric(df['accuracy_percent'], errors='coerce') +  pd.to_numeric(df['btt_rate'], errors='coerce') + pd.to_numeric(df['twp_rate'], errors='coerce') + pd.to_numeric(df['pressure_to_sack_rate'], errors='coerce'))  / 4

            # Remove the unnecessary columns
            columns_to_remove = ['accuracy_percent', 'btt_rate', 'twp_rate', 'pressure_to_sack_rate']
            df.drop(columns=columns_to_remove, inplace=True)

            df['year'] = year

            # Merge 'fantasy_rank' column from df_fantasy based on 'player'
            df = df.merge(df_fantasy[['Player', 'Fantasy Points Pts']], left_on='player', right_on='Player', how='left')

            # Drop the redundant 'Player' column from the merged DataFrame
            df.drop('Player', axis=1, inplace=True)

            # Save the filtered and selected data to a new CSV file
            df.to_csv('filtered_data/passing_stats_filtered_' + year +'.csv', index=False)


    def read_csvs(self, stat_csvs, results):
        ''' 
        The purpose of this method is to take the csvs in stat_csvs and output  a dictionary in the format below, this will be used to create test data with inputs of equal length.
        Data is padded if need be.

        state_csvs => A list of csv file names to be read
        results => THIS CAN BE REMOVED  dictionary... in the following format {team , { position , {player_name :[p_1_stats], player_name : [p_2_stats] } } }

        >>> test_obj = DATA_PROCESSOR()
        >>> length_comparator = 0
        >>> test_csv = test_obj.read_csvs(['filtered_data/receiving_stats_filtered_2022.csv'], {})
        >>> for team in test_csv:
        ...     for position in test_csv[team]:
        ...         for player in test_csv[team][position]:
        ...             len_player_list = len(test_csv[team][position][player])
        ...             if length_comparator == 0:
        ...                 length_comparator = len_player_list
        ...             elif len_player_list != length_comparator:
        ...                 print('false') 

        >>> length_comparator = 0
        >>> test_csv = test_obj.read_csvs(['filtered_data/receiving_stats_filtered_2022.csv', 'filtered_data/rushing_stats_filtered_2022.csv'], {})
        >>> for team in test_csv:
        ...     for position in test_csv[team]:
        ...         for player in test_csv[team][position]:
        ...             len_player_list = len(test_csv[team][position][player])
        ...             if length_comparator == 0:
        ...                 length_comparator = len_player_list
        ...             elif len_player_list != length_comparator:
        ...                 print(length_comparator            )
        ...                 print(len(test_csv[team][position][player]))
        ...                 break

        >>> length_comparator = 0
        >>> test_csv = test_obj.read_csvs(['filtered_data/receiving_stats_filtered_2022.csv', 'filtered_data/passing_stats_filtered_2022.csv', 'filtered_data/rushing_stats_filtered_2022.csv'], {})
        >>> for team in test_csv:
        ...     for position in test_csv[team]:
        ...         for player in test_csv[team][position]:
        ...             len_player_list = len(test_csv[team][position][player])
        ...             if length_comparator == 0:
        ...                 length_comparator = len_player_list
        ...             elif len_player_list != length_comparator:
        ...                 print(length_comparator)
        ...                 print(player)
        ...                 print(len(test_csv[team][position][player]))
        ...                 break

        >>> test_csv = test_obj.read_csvs(['filtered_data/receiving_stats_filtered_2022.csv', 'filtered_data/passing_stats_filtered_2022.csv', 'filtered_data/rushing_stats_filtered_2022.csv'], {})
        >>> print(test_csv['KC'])
        
        '''
        running_stat_len = 0

        for stat_csv in stat_csvs:

            # ave length of the lists
            player_stat_len = 0

            # Open the CSV file in read mode
            with open(stat_csv, 'r') as file:
                # Create a CSV reader object using DictReader
                reader = csv.DictReader(file)

                # Read the headers from the first row
                headers = reader.fieldnames

                # Iterate over the remaining rows
                for row in reader:
                    #if team does not exist yet add it
                    if not (row['team_name'] in results):
                        #add position
                        results[row['team_name']] = {}
                    #If position does not exist... add it 
                    if not (row['position'] in results[row['team_name']]):
                        # add new queue to position, which will also create position
                        results[row['team_name']][row['position']] = {}
                    if not (row['player'] in results[row['team_name']][row['position']]):
                        #add player id
                        results[row['team_name']][row['position']][row['player']] = []

                    #now that we know the dict is set up for our data we can add it
                    #loop through row and make a list of input data
                    input_data =[]
                    # print(row)
                    for key in row:
                        if key != 'player' and key != 'player':
                            input_data.append(row[key])
                 
                    this_player_length = len(results[row['team_name']][row['position']][row['player']])

                    #add to queue of player data... if there already is a value, merge the lists
                    if results[row['team_name']][row['position']][row['player']]: #if this returns [], this is false, so if value exists

                        #if len of this list is not equivalent to the last we must adjust it before extension
                        if ( (this_player_length + len(input_data)) < running_stat_len):
                            needed_length = running_stat_len - this_player_length
                            needed_list = [""] * needed_length
                            # blank spots go before current addition. We want old data in the same place
                            results[row['team_name']][row['position']][row['player']] + needed_list + input_data
                        else:
                            results[row['team_name']][row['position']][row['player']] + input_data
                    else:
                        #if we are on a second or third csv... there will be blank values ... but we need equal lenght data
                        #We shall enter ['' for x in len(lists)]
                        #there is no previous data... we want to add to to the end of a blank list of the old length
                        if ( (this_player_length + len(input_data)) < running_stat_len):
                            needed_length = running_stat_len - this_player_length  #minus one because player name will be going to the front
                            needed_list = [""] * needed_length
                            results[row['team_name']][row['position']][row['player']] = [row['player']] + needed_list + input_data
                        else:
                            results[row['team_name']][row['position']][row['player']] = [row['player']] + input_data
                
            # Now that we are onto the next csv... add to running length
            running_stat_len += this_player_length

        return results
    

    def make_input_output(self, stat_dict):
        '''
        The purpose of this method is to take the dictionary in the following format, and convert it to input and ouput lists appropriate for the model.

        {team , { position , {player name :[p_1_stats], player_name : [p_2_stats] } } }
        '''

        #store inputs/outputs
        inputs = []
        outputs = []




    
                
#debug

if __name__ == '__main__':

    #####################################################################
    #                           arguments
    #####################################################################

    parser = argparse.ArgumentParser()
    parser.add_argument('--method', help='pass method name to doctest this specific case')
    parser.add_argument('--sort_csv_years', help='pass the years of data to sort, this will call sort_csv on the receiving_data_<YEAR> and rushing_data_<YEAR>')

    args = parser.parse_args()
    #####################################################################

    #Initialize the class
    proc = DATA_PROCESSOR()


    ##Run Doctests
    if args.method:
        doctest.run_docstring_examples(getattr(DATA_PROCESSOR, args.method), globals(), optionflags=doctest.ELLIPSIS)
    elif args.sort_csv_years:
        print('\033[91mSorting Specified CSVS\033[0m')
        for year in args.sort_csv_years:
            print('\033[95mSorting for Year ' +year+ '\033[0m')
            proc.sort_csvs("receiver", year)
            proc.sort_csvs("rush", year)
    else:
        obj = DATA_PROCESSOR()
        obj.read_csvs(['test.csv'], {})

                    
